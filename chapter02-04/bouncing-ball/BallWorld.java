import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The world in which the ball bounces.
 * 
 * @author altfathv@email.sc.edu 
 * @version HW3
 */
public class BallWorld extends World
{ 
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public BallWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 

        prepare();
    } // end constructor BallWorld

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        Ball ball = new Ball();
        addObject(ball, Greenfoot.getRandomNumber(601), Greenfoot.getRandomNumber(401));
        Ball ball2 = new Ball();
        addObject(ball2, Greenfoot.getRandomNumber(601), Greenfoot.getRandomNumber(401));
        Ball ball3 = new Ball();
        addObject(ball3, Greenfoot.getRandomNumber(601), Greenfoot.getRandomNumber(401));
    }
} //end BallWorld class
