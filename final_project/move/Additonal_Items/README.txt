INSTRUCTIONS FOR MOVE

Character Selection:
When prompted on startup, enter the number 1 to play as the red character, or number 2 to play as the blue character. Press run again. 

Movement:
Use the up, down, left, and right arrow keys to control the character.

Objectives:
Collect all the dots before the timer runs out!