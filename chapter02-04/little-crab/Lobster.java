import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Lobster here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Lobster extends Actor
{
    /**
     * Act - do whatever the Lobster wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
     /**
     * Act - do whatever the lobster wants to do. This method is called whenever
     * the 'act' or 'run' button gets pressed in the environment.
     */
    public void act()
    {
        // If the crab intersects with the boundary of the world,
        // make it turn enough so that it can keep moving.
        turnAtEdge();

        // turn randomly 10% of time
        randomTurn();
        
        // attempt to move forward 5 pixels
        move(5);
        
        // eat a worm if in contact with one
        lookForCrab();
       
    } // end method act()

    /** Check whether we have stumbled upon a crab.
     * If we have, eat it. If not, do nothing.
     */
    public void lookForCrab()
    { 
        if ( isTouching(Crab.class) )
        { 
            removeTouching(Crab.class);
            Greenfoot.playSound("au.wav");
            Greenfoot.stop();
        } // end if
    } // end method lookFor

    /** Make the lobster turn by 45 degrees at random, 
     * ten percent of the time. 
     */
    public void randomTurn()
    {
        if ( Greenfoot.getRandomNumber( 100 ) < 10 )
        {
            turn( -(Greenfoot.getRandomNumber(91) -45 ) ); // approx turn 1 every 10 cycles

        } //end if
    } // end method randomTurn()
    
    /** Make the lobster turn 17 degrees when it encounters
     * an edge.
     */
    public void turnAtEdge()
    {
        if ( isAtEdge() )
        { 
            turn(17);
            
        } // end if
    } //end method turnAtEdge()
} // end lobster
