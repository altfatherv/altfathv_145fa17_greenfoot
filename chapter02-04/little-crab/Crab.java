import greenfoot.*;

/**
 * This class defines a crab. Crabs live on the beach.
 */
public class Crab extends Actor
{
    /**
     * Act - do whatever the crab wants to do. This method is called whenever
     * the 'act' or 'run' button gets pressed in the environment.
     */
    public void act()
    {

        // move crab left or right 5 degrees on keypress
        checkKeypress();
        
        // attempt to move forward 5 pixels
        move(5);
        
        // eat a worm if in contact with one
        lookForWorm();
       
    } // end method act()

    /** Check whether we have stumbled upon a worm.
     * If we have, eat it. If not, do nothing.
     */
    public void lookForWorm()
    { 
        if ( isTouching(Worm.class) )
        { 
            removeTouching(Worm.class);
            Greenfoot.playSound("slurp.wav");
        } // end if
    } // end method lookFor

    /** Turn the crab left or right 5 degees when the
     * corresponding key is pressed.
     */
    public void checkKeypress()
    {
        if ( Greenfoot.isKeyDown("left") )
        { turn(-4);
            
        } //end if
        
        if ( Greenfoot.isKeyDown("right") )
        { turn(4);
            
        } // end if
    } // end method checkKeypress
}// end  crab
