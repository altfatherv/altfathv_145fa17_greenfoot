import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * This class defines a crab. Crabs live on the beach. They like sand worms 
 * (very yummy, especially the green ones).
 * 
 * @author: altfathv@email.sc.edu
 * @version: HW 3
 * 
 * In this version, the crab behaves as before, but we add animation of the 
 * image.
 */

public class Crab extends Actor
{
    /* FILDS (INSTANCE VARIABLES) */
    private GreenfootImage image1;
    private GreenfootImage image2;
    private int wormsEaten;

    /* CONSTRUCTORS */
    /**
     * Create a crab and initialize its two images.
     */
    public Crab()
    {
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png");
        setImage(image1);
        wormsEaten = 0;
    } // end Crab constructor

    /* METHODS */
    /** 
     * Act - do whatever the crab wants to do. This method is called whenever
     *  the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        checkKeypress();
        move(8);
        lookForWorm();
        switchImage();

    } //end method act()

    /**
     * Alternate the crab's image between image1 and image2.
     */
    public void switchImage()
    { 
        /* If the Crab is assigned image1, switch it to image 2 and vice versa. */
        
        if (getImage() == image1) 
        {
            setImage(image2);
        } 
        else
        {
            setImage(image1);
        } // end if-else
    } //end method switchImage()

    /**
     * Check whether a control key on the keyboard has been pressed.
     * If it has, react accordingly.
     */
    public void checkKeypress()
    { /* Check to see if the left or right arrow keys are pressed down 
       * and turn the crab 4 degrees accordingly
       */

        if (Greenfoot.isKeyDown("left")) 
        {
            turn(-4);
        } // end if
        if (Greenfoot.isKeyDown("right")) 
        {
            turn(4);
        } // end if
    } // end method checkKeypress()

    /**
     * Check whether we have stumbled upon a worm.
     * If we have, eat it. If not, do nothing. If we have
     * eaten eight worms, we win.
     */
    public void lookForWorm()
    {
        /* Check to see if the Crab is touching a worm, if so remove the Worm 
         * and play slurp.wav. Additionally, add 1 to wormsEaten each time a Worm is removed. 
         * When the Crab has eaten 8 Worms, end the game and play fanfare.wav 
         */
         
        if ( isTouching(Worm.class) ) 
        {

            removeTouching(Worm.class);
            Greenfoot.playSound("slurp.wav");

            wormsEaten = wormsEaten + 1;

            if (wormsEaten == 8) 
            {

                Greenfoot.playSound("fanfare.wav");
                Greenfoot.stop();

            } // end if
        } // end if
    } // end method lookForWorm()
} // end crab class