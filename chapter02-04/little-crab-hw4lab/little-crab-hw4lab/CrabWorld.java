import greenfoot.*;  // imports Actor, World, Greenfoot, GreenfootImage

/**
 * The CrabWorld is the place where crabs and other creatures live. 
 * It creates the initial population.
 *  
 * @author your_username@email.uscb.edu
 * @version 145fa17_hw4lab
 */
public class CrabWorld extends World
{
    /* FIELDS */
    private Crab crab; // remember: the CrabWorld "has-a" Crab object
                       // (this is an example of a COMPOSITION relationship)
    private int numberOfWorms;
    private int numberOfLobsters;
    /* CONSTRUCTORS */
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        super(560, 560, 1); //always first line of constructor
        
        String strNumberofWorms = 
            Greenfoot.ask("How Many Worms? (Enter a number between 8-20): ");
         
        String strNumberofLobsters = 
            Greenfoot.ask("How Many Lobsters (Enter a number between 1-5): ");
        // convert string values to their numeric (int) equivalents
        numberOfWorms = Integer.parseInt( strNumberofWorms);
        numberOfLobsters = Integer.parseInt( strNumberofLobsters);    
        prepare();
    } // end no-arg constructor for CrabWorld

    /* METHODS */
    public void act() 
    {
        showText( "Worms eaten: " + crab.getWormsEaten(), 100, 50 );
        
        if ( crab.getWormsEaten() == 8 )
        {
            Greenfoot.playSound("fanfare.wav");
            Greenfoot.stop();
        } // end if
    } // end method act
    
    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        crab = new Crab();
        addObject(crab, 231, 203);
        
        /*
        Worm worm = new Worm();
        addObject(worm, 445, 137);
        Worm worm2 = new Worm();
        addObject(worm2, 454, 369);
        Worm worm3 = new Worm();
        addObject(worm3, 368, 466);
        Worm worm4 = new Worm();
        addObject(worm4, 129, 488);
        Worm worm5 = new Worm();
        addObject(worm5, 254, 388);
        Worm worm6 = new Worm();
        addObject(worm6, 106, 334);
        Worm worm7 = new Worm();
        addObject(worm7, 338, 112);
        Worm worm8 = new Worm();
        addObject(worm8, 150, 94);
        Worm worm9 = new Worm();
        addObject(worm9, 373, 240);
        Worm worm10 = new Worm();
        addObject(worm10, 509, 55);
        */
       
       // use a counter-controlled for loop to spawn the worms
       // in random locations in the CrabWorld.
       
       for ( int wormIndex = 0; wormIndex < numberOfWorms; wormIndex++ )
        {
            int xWorm = Greenfoot.getRandomNumber( 531 ) + 15;
            int yWorm = Greenfoot.getRandomNumber( 531 ) + 15;
            addObject( new Worm(), xWorm, yWorm );
        } //end for 
       
        /* Lobster lobster = new Lobster();
        addObject(lobster, 334, 65);
        Lobster lobster2 = new Lobster();
        addObject(lobster2, 481, 481);
        Lobster lobster3 = new Lobster();
        addObject(lobster3, 79, 270);*/
        
       // use a counter-controlled for loop to spawn the worms
       // in random locations in the CrabWorld.
       
       for ( int lobsterIndex = 0; lobsterIndex < numberOfLobsters; lobsterIndex++ )
        {
            int xLobster = (int)( Math.random() * 531 )+15; // Greenfoot.getRandomNumber( 531 ) + 15;
            int yLobster = (int)( Math.random() * 531 )+15; // Greenfoot.getRandomNumber( 531 ) + 15;
            addObject( new Lobster(), xLobster, yLobster );
        } //end for
    } // end method prepare
} // end class CrabWorld