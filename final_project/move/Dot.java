import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A dot. Players collect these to win.
 * 
 * @author altfathv
 * @version 1
 */
public class Dot extends Actor
{
    /**
     * Act - do whatever the Dot wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    } //end act()
} //end class Dot
