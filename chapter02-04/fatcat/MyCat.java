import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * MyCat is your own cat. Get it to do things by writing code in its act method.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyCat extends Cat
{
    /**
     * Act - do whatever the MyCat wants to do.
     */
    public void act()
    {

        if ( isSleepy() ) // cat sleeps for a short amount of time if it is sleepy.
        {
            sleep(1);
        } // end if
        else 
        {
            shoutHooray();
        } //end else

        if ( isHungry() ) // cat eats if it is hungry.
        {
            eat();
        } // end if

        if ( isBored() ) // cat dances for a short amount of time if it is bored.
        // seems to override eat() if placed before.
        {
            dance();
        } // end if

        if ( isAlone() ) // cat will sleep if alone and shout hooray otherwise
        {
            sleep(1);
        }
        else
        {
            shoutHooray();
        }
    }   //end act method
} //end class MyCat
