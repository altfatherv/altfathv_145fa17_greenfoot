import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * A lobster. Lobsters live on the beach. They like to eat crabs. 
 * 
 * @author: altfathv@email.sc.edu
 * @version: HW 3
 * 
 * The lobster walks around randomly. If it runs into a crab it eats it.
 * In this version, we have added a sound effect, and the game stops when
 * a lobster eats the crab.
 */

public class Lobster extends Actor
{
    /**
     * Lobster moves around the screen searching for crabs to eat.
     */
    public void act()
    {
        turnAtEdge();
        randomTurn();
        move(5);
        lookForCrab();
        turnTowardsCenter();
    } //end method act()

    /**
     * Check whether we are at the edge of the world. If we are, turn a bit.
     * If not, do nothing.
     */
    public void turnAtEdge()
    {
        /* If the Lobster is at the edge of the world, turn it by 17 degrees. */
        
        if ( isAtEdge() ) 
        {
            turn(17);
        } //end if
    } //end method isAtEdge()

    /**
     * Randomly decide to turn from the current direction, or not. If we turn
     * turn a bit left or right by a random degree.
     */
    public void randomTurn()
    {
        /* 10% of the time, turn the Lobster a value between -45 and 45*/
        
        if (Greenfoot.getRandomNumber(100) > 90) 
        {
            turn(Greenfoot.getRandomNumber(91)-45);
        } //end if
    } //end method randomTurn()

    
    /**
     * Randomly decide to turn towards the center of the screen or not.
     */
    public void turnTowardsCenter()
    {
        /* Turn the Lobster towards the center of the screen 1% of the time */
        
        if (Greenfoot.getRandomNumber(100) > 99) 
        {
            turnTowards(280, 280);
        } //end if
    } //end method turnTowardsCenter()
    
    /**
     * Try to pinch a crab. That is: check whether we have stumbled upon a crab.
     * If we have, remove the crab from the game, and stop the program running.
     */
    public void lookForCrab()
    {
        /* If the Lobster is touching a Crab: remove the Crab, play au.wav, and end the game. */
        
        if ( isTouching(Crab.class) ) 
        {
            removeTouching(Crab.class);
            Greenfoot.playSound("au.wav");
            Greenfoot.stop();
        } //end if
    } //end method lookForCrab()
} // end class Lobster