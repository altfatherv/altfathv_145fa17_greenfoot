import greenfoot.*;  // (Actor, World, Greenfoot, GreenfootImage)

public class CrabWorld extends World
{
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        super(560, 560, 1);

        // instantiates new a new crab object
        // then assigns a reference to that object to
        // a new variable called myCrab, which is of the Crab data type
        Crab myCrab = new Crab();

        // add a object referenced by myCrab
        // to the world at the indicated coordinates.
        addObject( myCrab, 250, 200);
        prepare();
    } //end CrabWorld constructor
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        Lobster lobster = new Lobster();
        addObject(lobster,509,494);
        Lobster lobster2 = new Lobster();
        addObject(lobster2,46,526);
        Lobster lobster3 = new Lobster();
        addObject(lobster3,502,25);
        
        
        
        Worm worm = new Worm();
        addObject(worm,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
        Worm worm2 = new Worm();
        addObject(worm2,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
        Worm worm3 = new Worm();
        addObject(worm3,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
        Worm worm4 = new Worm();
        addObject(worm4,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
        Worm worm5 = new Worm();
        addObject(worm5,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
        Worm worm6 = new Worm();
        addObject(worm6,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
        Worm worm7 = new Worm();
        addObject(worm7,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
        Worm worm8 = new Worm();
        addObject(worm8,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
        Worm worm9 = new Worm();
        addObject(worm9,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
        Worm worm10 = new Worm();
        addObject(worm10,(Greenfoot.getRandomNumber (560) + 1),(Greenfoot.getRandomNumber (560) + 1));
    }
} // end class CrabWorld