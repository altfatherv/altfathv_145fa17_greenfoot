import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Vertical Mazewall that players are unable to pass through.
 * 
 * @author altfathv
 * @version 1
 */
public class Mazewallb extends Actor
{
    /**
     * Act - do whatever the MazewallB wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    } // end act method  
} //end class Mazewallb
