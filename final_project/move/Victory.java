import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The word "victory". Appears when all dots are collected.
 * 
 * @author altfathv
 * @version 1
 */
public class Victory extends Actor
{
    /**
     * Act - do whatever the Victory wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    } //end act()
} //end class Victory