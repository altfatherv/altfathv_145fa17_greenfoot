import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Mazewall that players are unable to pass through.
 * 
 * @author altfathv
 * @version 1
 */
public class Mazewall extends Actor
{
    /**
     * Act - do whatever the Mazewall wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    } //end act()
} //end class Mazewall
