import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Gates control which characters can pass into an area. 
 * Player2 is unable to pass through gates.
 * 
 * @author altfathv 
 * @version 1
 */
public class Gate extends Actor
{
    /**
     * Act - do whatever the gate wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }//end act()
} //end class Gate
