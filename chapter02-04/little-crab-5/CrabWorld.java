 import greenfoot.*;  // imports Actor, World, Greenfoot, GreenfootImage

/**
 * @author: altfathv@email.sc.edu
 * @version: HW 3
 * 
 * The CrabWorld is the place where crabs and other creatures live. 
 * It creates the initial population.
 */
public class CrabWorld extends World
{
    /* INSTANCE VARIABLES */
    private int crabCounter;

    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        super(560, 560, 1);
        prepare();
        crabCounter=500;

    } // end CrabWorld() constructor

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        Crab crab = new Crab();
        addObject(crab, 200, 200);
        Worm worm = new Worm();
        addObject(worm, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Worm worm2 = new Worm();
        addObject(worm2, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Worm worm3 = new Worm();
        addObject(worm3, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Worm worm4 = new Worm();
        addObject(worm4, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Worm worm5 = new Worm();
        addObject(worm5, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Worm worm6 = new Worm();
        addObject(worm6, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Worm worm7 = new Worm();
        addObject(worm7, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Worm worm8 = new Worm();
        addObject(worm8, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Worm worm9 = new Worm();
        addObject(worm9, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Worm worm10 = new Worm();
        addObject(worm10, Greenfoot.getRandomNumber(561), Greenfoot.getRandomNumber(561));
        Lobster lobster = new Lobster();
        addObject(lobster, 334, 65);
        Lobster lobster2 = new Lobster();
        addObject(lobster2, 481, 481);
        Lobster lobster3 = new Lobster();
        addObject(lobster3, 81, 11);
    } //end prepare() method

    /**
     * Track the lifespan of the crab, display time left, and prompt a game over when time
     * runs out.
     */
    public void act()
    {
        crabCounter = crabCounter -1;
        timeOut();
        showText("TIME LEFT: "+ crabCounter, 100, 400);
    } // end method act()

    public void timeOut()
    { /* If crabCounter reaches 0, stop the game and play a game over sound. */
      if ( crabCounter == 0)
      {
        Greenfoot.playSound("game-over.wav");
        Greenfoot.stop();    
      }
    } // end method timeOut();
} // end class CrabWorld