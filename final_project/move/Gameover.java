import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The word "gameover". Appears when the timer runs out.
 * 
 * @author altfathv 
 * @version 1
 */
public class Gameover extends Actor
{
    /**
     * Act - do whatever the Gameover wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    } //end act()   
} //end class Gameover
