import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The red player character.
 * 
 * This character can pass through gates and has no speed reduction.
 * 
 * @author altfathv
 * @version 1
 */
public class Player extends Actor
{
    /* INSTANCE VARIABLES */
    private int energy;
    private int dotsCollected;

    /* CONSTRUCTOR */
    /**
    * Player constructor. Set variables.
    */
    public Player()
    {
        dotsCollected = 0;

    } //end player constructor

    /**
     * Act - call movement() and collectDots() methods.
     */
    public void act() 
    {
        movement();
        collectDots();

    } //end method act() 

    /** Checks to see if an arrow key is being pressed. 
     * If so, it moves a certain amount of pixels in that
     * direction. Additionally, if the player is in contact with a maze wall,
    their position will be set to it's current one, preventing it from moving. */

    public void movement()
    {
        int x = getX(), y = getY();

        if (Greenfoot.isKeyDown("up")) //move up ten pixels
        {
            setLocation(getX(), getY()-12);
            energy = energy - 1;
        } //end if

        if (Greenfoot.isKeyDown("down")) //move down 10 pixels
        {
            setLocation(getX(), getY()+12);
            energy = energy - 1;
        } //end if

        if (Greenfoot.isKeyDown("left")) //move left 10 pixels
        {
            setLocation(getX()-12, getY());
            energy = energy - 1;
        } //end if

        if (Greenfoot.isKeyDown("right")) //move right 10 pixels
        {
            setLocation(getX()+12, getY());
            energy = energy - 1;
        } //end if

        /* Idea for wall collision using isTouching() and x and y variables originally
         * from greenfoot.org user Super_Hippo (source: https://www.greenfoot.org/topics/56877/0)
         * I wanted to implement wall collision with the least amount of code and this seemed
         * like a good method. */
        if (isTouching(Mazewall.class)) //If the Player is touching a wall, its location is set back to its current one
        {
            setLocation(x, y);
        }  //end if

        if (isTouching(Mazewallb.class)) //If the Player is touching a wall, its location is set back to its current one
        {
            setLocation(x, y);
        }  //end if
    }   //end method checkKeyPress

    /**
     * Dots will disappear when the Player character collides with them and the
     * dotsCollected variable is incremented by 1. Once 10 dots are collected, 
     * the game is stopped and a victory screen is displayed.
     */
    public void collectDots()
    {
        if ( isTouching(Dot.class) ) 
        {
            removeTouching(Dot.class);
            dotsCollected = dotsCollected + 1;

            if ( dotsCollected == 24) //for some reason it ends the game when exactly
            // half of the number dots specified are collected.
            {
                Greenfoot.playSound("fanfare.wav");
                Greenfoot.stop();
                Victory victory = new Victory();
                getWorld().addObject(victory, 250, 250); 
            } // end if 
        } // end if
    } // end method collectDots()
} //end class player