import greenfoot.*;  // imports Actor, World, Greenfoot, GreenfootImage

/**
 * The CrabWorld is the place where crabs and other creatures live. 
 * It creates the initial population.
 *  
 * @author your_username@email.uscb.edu
 * @version 145fa17_hw4lab
 */
public class CrabWorld extends World
{
    /* FIELDS */
    private Crab crab; // remember: the CrabWorld "has-a" Crab object
                       // (this is an example of a COMPOSITION relationship)
                       
    private int numberOfWorms;
    private int numberOfLobsters;

    private boolean setupStatus; // specifies the "state" of the game (setting-up vs. playing)

    /* CONSTRUCTORS */
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        super(560, 560, 1); // this always needs to be the first line of a constructor
        setupStatus = true; // initialize this boolean "status variable" for controlling
                            // what happens during the act method below
    } // end no-arg constructor for CrabWorld

    /* METHODS */
    /**
     * This is the act method for the CrabWorld. It will be called once
     * per cycle through the "game loop", so it functions similarly to
     * the act method for Actor and its subclasses.
     */
    public void act() 
    {
        /*
         * Note: for the first pass through the "game loop," we will
         * set up the scenario by asking the user to enter data that 
         * will be used to set the number of worms and lobsters in 
         * the game. We start by checking the value of the boolean instance 
         * variable, setupStatus. If setupStatus is true, then we
         * will execute the code for setting up the game world -- but since
         * we don't want to "keep setting up the world" for subsequent
         * act method cycles, we immediately set the value of setupStatus 
         * to false. As a result, the statements inside the if statement
         * will only execute once, during the first act method cycle.
         * 
         * PROGRAMMING TIP: Study how this boolean "status variable" is used here.
         * You may find that similar "status variables" can help you, as a programmer,
         * maintain control of how the scenario runs from one act method to the
         * next. (Remember, the act method is repeatedly being called, once per
         * each cycle through the "game loop.")
         */
        if ( setupStatus ) // more efficient than "if ( setupStatus == true )"
        {
            String strNumberOfWorms =
                Greenfoot.ask("How many worms? (Enter a number between 8 and 20): ");

            String strNumberOfLobsters =
                Greenfoot.ask("How many lobsters? (Enter a number between 1 and 5): "); 

            // convert String values to their numeric (int) equivalents
            numberOfWorms = Integer.parseInt( strNumberOfWorms );
            numberOfLobsters = Integer.parseInt( strNumberOfLobsters );

            // call the prepare method to add objects to the world
            prepare();
            
            // pause the game after the worm and lobster objects are added 
            // to the world. (The user must then click the "Run" button to play.)
            Greenfoot.stop();
            
            setupStatus = false; // prevents "setting up" during subsequent
                                 // calls to the act method
        } // end if

        /*
         * The rest of the statements in the act method are not dependent
         * on setupStatus. Thus, as coded here, they will be executed
         * during every cycle through the "game loop."
         */
        showText( "Worms eaten: " + crab.getWormsEaten(), 100, 50 );

        if ( crab.getWormsEaten() == 8 )
        {
            Greenfoot.playSound("fanfare.wav");
            Greenfoot.stop();
        } // end if
    } // end method act

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {

        crab = new Crab();
        addObject(crab, 231, 203);
        /*
        Worm worm = new Worm();
        addObject(worm, 445, 137);
        Worm worm2 = new Worm();
        addObject(worm2, 454, 369);
        Worm worm3 = new Worm();
        addObject(worm3, 368, 466);
        Worm worm4 = new Worm();
        addObject(worm4, 129, 488);
        Worm worm5 = new Worm();
        addObject(worm5, 254, 388);
        Worm worm6 = new Worm();
        addObject(worm6, 106, 334);
        Worm worm7 = new Worm();
        addObject(worm7, 338, 112);
        Worm worm8 = new Worm();
        addObject(worm8, 150, 94);
        Worm worm9 = new Worm();
        addObject(worm9, 373, 240);
        Worm worm10 = new Worm();
        addObject(worm10, 509, 55);
         */

        // use a counter-controlled for loop to 
        // spawn the worms, in random locations, in the CrabWorld
        for ( int wormIndex = 0; wormIndex < numberOfWorms; wormIndex++ )
        {
            int xWorm = Greenfoot.getRandomNumber( 531 ) + 15; 
            int yWorm = Greenfoot.getRandomNumber( 531 ) + 15; 
            addObject( new Worm(), xWorm, yWorm ); 
        } // end for
        /*
        Lobster lobster = new Lobster();
        addObject(lobster, 334, 65);
        Lobster lobster2 = new Lobster();
        addObject(lobster2, 481, 481);
        Lobster lobster3 = new Lobster();
        addObject(lobster3, 79, 270);
         */

        // use a counter-controlled for loop to 
        // spawn the lobsters, in random locations, in the CrabWorld
        for ( int lobsterIndex = 0; lobsterIndex < numberOfLobsters; lobsterIndex++ )
        {
            int xLobster = (int)(Math.random() * 531) + 15;  //Greenfoot.getRandomNumber( 531 ) + 15; 
            int yLobster = (int)(Math.random() * 531) + 15;  //Greenfoot.getRandomNumber( 531 ) + 15; 
            addObject( new Lobster(), xLobster, yLobster ); 
        } // end for

    } // end method prepare
} // end class CrabWorld