import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Balls bounce around the screen.
 * 
 * @author altfathv@email.sc.edu
 * @version HW3
 */
public class Ball extends Actor
{/* INSTANCE VARIABLES */
    private int bounceCount;

    public Ball() 
    {
        bounceCount=0;
    } // end Ball() constructor

    /**
     * Act - do whatever the Ball wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move(5);
        bounce();
    } // end method act

    /**
     * Ball will go the opposite direction if it hits a wall.
     * 
     */
    public void bounce()
    {
        /* If Ball is at the edge of the world, turn it between -180 and 180 degrees,
        as well as add 1 to bounceCount. 
         */
        if ( isAtEdge() ) 
        {
            turn(Greenfoot.getRandomNumber(360)-180);
            bounceCount=bounceCount + 1;

        } //end if
    } //end method bounce()

} // end class Ball
