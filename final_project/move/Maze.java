import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Maze here.
 * 
 * @author altfathv 
 * @version 1
 */
public class Maze extends World
{
    /* INSTANCE VARIABLES */
    private Player player;
    private int timer;
    private int charSelect;
    private boolean setupStatus;

    /**
     * Constructor for class Maze.
     * Create the world, set the timer, set setupStatus to true,
     * as well as run the prepare() method to create player characters.
     */
    public Maze()
    {    
        // Create a new world with 500x500 cells with a cell size of 1x1 pixels.
        super(500, 500, 1); 
        timer = 425;
        setupStatus = true;
        prepare();
    } //end maze constructor

    /**
     * Act -- Keep track of the time and determine the setupStatus. If the game is in set-up mode,
     * prompt the player to select which character they want to play as.
     */
    public void act()
    {
        timer = timer - 1;
        gameStatus();
        showText("TIME LEFT: "+ timer , 80, 20);
        /* setStatus and prepare() code from Dr. Canada's Little Crab HW 4 Lab fix on BlackBoard */
        if ( setupStatus ) // more efficient than "if ( setupStatus == true )"
        {
            String strCharSelect=
                Greenfoot.ask("Select a Character! Press 1 for Red and 2 for Blue: ");

            // convert String to int
            charSelect = Integer.parseInt( strCharSelect );

            // add players
            prepare();

            // pause the game after Player/Player 2 is added 
            // to the world. click run to play.
            Greenfoot.stop();

            setupStatus = false;
        } // end if

    } // end method act()

    /**
     *  Add the maze, dots, and player characters into the game.
     */
    private void prepare()
    {
        player = new Player();

        if ( charSelect == 1 )
        {
            addObject( new Player(), 200, 0); 
        } // end if

        if ( charSelect == 2 )
        {
            addObject( new Player2(), 200, 0); 
        } // end if

        /* Added indentation for readability */
        
        /* HORIZONTAL MAZEWALLS */
        Mazewall mazewall = new Mazewall();
            addObject(mazewall, 253,195);
        Mazewall mazewall2 = new Mazewall();
            addObject(mazewall2, 253,305);
        Mazewall mazewall3 = new Mazewall();
            addObject(mazewall3, 253,133);
        Mazewall mazewall4 = new Mazewall();
            addObject(mazewall4, 74,108);
        Mazewall mazewall6 = new Mazewall();
            addObject(mazewall6, 19,217);
        Mazewall mazewall7 = new Mazewall();
            addObject(mazewall7, 252,371);
        Mazewall mazewall8 = new Mazewall();
            addObject(mazewall8, 252,444);
        Mazewall mazewall9 = new Mazewall();
            addObject(mazewall9, 494,341);
        Mazewall mazewall10 = new Mazewall();
            addObject(mazewall10, 20,407);
        Mazewall mazewall11 = new Mazewall();
            addObject(mazewall11,498,130);
        Mazewall mazewall12 = new Mazewall();
            addObject(mazewall2, 73,22);
        Mazewall mazewall13 = new Mazewall();
            addObject(mazewall13,74,19);
        Mazewall mazewall14 = new Mazewall();
            addObject(mazewall14, 494,431);
        Mazewall mazewall15 = new Mazewall();
            addObject(mazewall15, 20,318);
        Mazewall mazewall16 = new Mazewall();
            addObject(mazewall16, 498,41);

        /* VERTICAL MAZEWALLS */
        Mazewallb Mazewallb = new Mazewallb();
            addObject(Mazewallb, 193,241);
        Mazewallb Mazewallb2 = new Mazewallb();
            addObject(Mazewallb2, 374,177);
        Mazewallb Mazewallb3 = new Mazewallb();
            addObject(Mazewallb3, 134,178);
        Mazewallb Mazewallb4 = new Mazewallb();
            addObject(Mazewallb4, 134,329);
        Mazewallb Mazewallb5 = new Mazewallb();
            addObject(Mazewallb5, 374,293);
        Mazewallb Mazewallb6 = new Mazewallb();
            addObject(Mazewallb6, 374,87);
        Mazewallb Mazewallb7 = new Mazewallb();
            addObject(Mazewallb7, 466,233);
        Mazewallb Mazewallb8 = new Mazewallb();
            addObject(Mazewallb8, 257,24);
        Mazewallb Mazewallb9 = new Mazewallb();
            addObject(Mazewallb9, 376,470);
        Mazewallb Mazewallb10 = new Mazewallb();
            addObject(Mazewallb10, 134,485);
        
        /* GATES */
        Gate gate = new Gate();
            addObject(gate,312,238);
        Gate gate2 = new Gate();
            addObject(gate2,312,260);
        Gate gate3 = new Gate();
            addObject(gate3,438,85);
        Gate gate4 = new Gate();
            addObject(gate4,133,63);
        Gate gate5 = new Gate();
            addObject(gate5,434,385);
        Gate gate6 = new Gate();
            addObject(gate6,80,362);
            
        /* DOTS */
        Dot dot = new Dot();
            addObject(dot, 473,90);
        Dot dot2 = new Dot();
            addObject(dot2, 60,68);
        Dot dot3 = new Dot();
            addObject(dot3, 42,377);
        Dot dot4 = new Dot();
            addObject(dot4, 485,389);
        Dot dot6 = new Dot();
            addObject(dot6, 63,490);
        Dot dot7 = new Dot();
            addObject(dot7, 250,244);
        Dot dot8 = new Dot();
            addObject(dot8, 307,70);
        Dot dot9 = new Dot();
            addObject(dot9, 246,490);
        Dot dot10 = new Dot();
            addObject(dot10, 459,481);
        Dot dot11 = new Dot();
            addObject(dot11, 88,159);
        Dot dot12 = new Dot();
            addObject(dot12, 494,228);
        Dot dot13 = new Dot();
            addObject(dot13, 237,403);

    } // end method prepare

    /**
     * Determines when the game will end, based on whether or not the timer is at 0.
     */
    public void gameStatus()
    { /* If timer reaches 0, stop the game and play a game over sound. */
        if ( timer == 0 )
        {
            Greenfoot.stop();    
            Greenfoot.playSound("game-over.wav");
            Gameover gameover = new Gameover();
            addObject(gameover, 250, 250);
        } //end if
    } // end method gameOver()
} //end class Maze
