import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * This class defines a crab. Crabs live on the beach. They like sand worms 
 * (very yummy, especially the green ones).
 * 
 * In this version, the crab behaves as before, but we add animation of the 
 * image.
 * 
 * @author your_username@email.uscb.edu
 * @version 145fa17_hw4lab
 */
public class Crab extends Actor
{
    /* FIELDS (INSTANCE VARIABLES) */
    private GreenfootImage image1;
    private GreenfootImage image2;
    private int wormsEaten;
    
    /* CONSTRUCTORS */
    /**
     * For a newly-instantiated Crab object, initialize
     * the state of that object (by initializing its instance variables)
     */
    public Crab()
    {
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png");
        setImage(image1);
        wormsEaten = 0;
    } // end no-arg constructor for Crab
    
    /* METHODS */
    /** 
     * Act - do whatever the crab wants to do. This method is called whenever
     *  the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        checkKeypress();
        move(5);
        lookForWorm();
        switchImage();
    } // end method act
    
    /**
     * Alternate the crab's image between image1 and image2.
     */
    public void switchImage()
    {
        if (getImage() == image1) 
        {
            setImage(image2);
        }
        else
        {
            setImage(image1);
        } // end if/else
    } // end method switchImage
            
    /**
     * Check whether a control key on the keyboard has been pressed.
     * If it has, react accordingly.
     */
    public void checkKeypress()
    {
        if (Greenfoot.isKeyDown("left")) 
        {
            turn(-4);
        } // end if
        
        if (Greenfoot.isKeyDown("right")) 
        {
            turn(4);
        } // end if
    } // end method checkKeypress
    

    
    /**
     * Check whether we have stumbled upon a worm.
     * If we have, then eat it (and increment the number of worms eaten.) 
     * If not, do nothing. 
     */
    public void lookForWorm()
    {
        if ( isTouching(Worm.class) ) 
        {
            removeTouching(Worm.class);
            Greenfoot.playSound("slurp.wav");           
            wormsEaten = wormsEaten + 1;
        } // end outer if
    } // end method lookForWorm
    
    /**
     * Returns the number of worms that have been eaten by the crab
     */
    public int getWormsEaten()
    {
        return wormsEaten;
    } // end method getWormsEaten
} // end class Crab